#include <SFML/Graphics.hpp>
#include <cstdlib>
#include <algorithm>
#include <iostream>
#include <vector>

enum class Pcs {
    K, Q, B, N, R, P,
    E // empty
};

enum class Team {
    W, B
};


enum class Actions {
    move,
    capture,

    en_passant,
    castle,
    promote,

    stale_mate,
    mate,

    premove,
    advanced_premove 

    // Advanced Premove
    /*
        if player A Guessed what piece Player B wanted to move
        he can premove his move
        Usefull in fast formats 
    */
};

enum class Game_State
{
    beggining,
    active,
    pause,
    draw,
    end
};

class Piece
{
public :
    Pcs figure{};
    Team team{};
    sf::Vector2u piece_position{};
    bool is_moved = false;
    bool is_promoted = false;
};

class Game {

public:

    void run() {
        init_sprite_textures();
        piece_cords();
        init_pieces();
        print_pieces();
        
        while (wnd.isOpen())
        {
            sf::Event event{};

            while (wnd.pollEvent(event))
            {
                if (event.type == sf::Event::Closed) {
                    wnd.close();
                }

                // printing pos of mouse - test purposes
                // std::cout << sf::Mouse::getPosition(wnd).x << " " << sf::Mouse::getPosition(wnd).y << std::endl;

                sprite_in_prison();

                drag(event);
                drop(event);
                // change tour
            }

            render();
        }
    }

    void init_sprite_textures();
    void piece_cords();
    void sprite_in_prison() const;
    void render();
    void init_pieces();
    void print_pieces();
    bool move_validation(
        Piece * sel_piece, 
        const sf::Vector2i sel_pos,
        const sf::Vector2u start_pos);
    void capture(std::vector<Piece> pieces);
    sf::Vector2u drag(const sf::Event event);
    void drop(const sf::Event event);
    void promote_pawn(std::vector<Piece> pieces);
    sf::Vector2i reverse_notation(const sf::Vector2i & start_pos);

private:
    sf::RenderWindow wnd;
    sf::Vector2i sel_pos = {};
    sf::Texture board, piece;
    
    sf::IntRect piece_im_cords[12];
    sf::Sprite sprite[3];

    Piece * sel_piece = nullptr;

    sf::Vector2u sheet_size = {};
    sf::Vector2u sprite_size = {};
    sf::Vector2u board_size = {};

    Game_State game_state = Game_State::beggining;
    Team tour = Team::W;
    sf::Vector2u start_pos;

    std::vector<Piece> pieces{};
};


sf::Vector2i reverse_notation(const sf::Vector2i & sel_pos)
{
    return sf::Vector2i{};
}

void Game::init_pieces() {
    
    pieces.clear(); 
    Pcs figure;

    for(int col = 0; col < 8; col++){
        for(int row = 0; row < 8; row++){
            
                 if(col == 4) figure = Pcs::K;
            else if(col == 3) figure = Pcs::Q;
            else if(col == 2 || col == 5) figure = Pcs::B;
            else if(col == 1 || col == 6) figure = Pcs::N;
            else if(col == 0 || col == 7) figure = Pcs::R;
            if(row == 1 || row == 6) figure = Pcs::P;

            pieces.push_back(
                Piece {
                    figure, 
                    row == 0 || row == 1 ? Team::W : Team::B, 
                    sf::Vector2u(col * 100, row * 100), 
                    false,
                    false
                }
            );

            // skips empty tiles into next team
            if(row == 1) row = 5;
        }
    }
}

// debug purposes, might delete later
void Game::print_pieces() {
    for (const auto& piece : pieces) {

        std::string figure;
             if(piece.figure == Pcs::B) figure = "Bish";
        else if(piece.figure == Pcs::N) figure = "Nigh";
        else if(piece.figure == Pcs::K) figure = "King";
        else if(piece.figure == Pcs::Q) figure = "Quen";
        else if(piece.figure == Pcs::R) figure = "Rook";
        else if(piece.figure == Pcs::P) figure = "Pawn";

        std::cout << "Figure: " << figure  << ", Team: "; 
        if (piece.team == Team::W) {
            std::cout << "White";
        } else {
            std::cout << "Black";
        }

        #if 0
        std::cout << ", piece_position: x = " << piece.piece_position.x/100 + 1 << 
           " y = " << piece.piece_position.y/100 + 1 << std::endl;
        #endif
    }
}

void Game::init_sprite_textures()
{
    piece.loadFromFile("../sprites/pieces.png");
    board.loadFromFile("../sprites/board.png");
    
    board_size = sf::Vector2u{
        board.getSize().x,
        board.getSize().y
    };

    sheet_size = sf::Vector2u{
        piece.getSize().x, 
        piece.getSize().y
    };

    sprite_size = sf::Vector2u{
        (piece.getSize().x)/6, 
        (piece.getSize().y)/2
    };

    wnd.create(sf::VideoMode(board_size.x, board_size.y), "Chess");

    sprite[0].setTexture(board);
    sprite[1].setTexture(piece);
    sprite[2].setTexture(piece);

    sf::Vector2f s = {0.75f, 0.75f};
    sprite[1].setScale(s);
    sprite[2].setScale(s);

    sprite[2].setOrigin(sprite_size.x/2.f,sprite_size.y/2.f);
    
    piece.setSmooth(true);    
}

void Game::piece_cords() {
    //Slicing pieces.png into each pawn
    for (int row = 0; row < 2; row++) {
        for (int col = 0; col < 6; col++) {
            piece_im_cords[(row * 6) + col] = 
                sf::IntRect(col * sprite_size.x,
                            row * sprite_size.y,
                            sprite_size.x,
                            sprite_size.y
                );
        }
    }
}

void Game::render()
{
    if (sel_piece) {
        sel_piece->piece_position = sf::Vector2u {
            static_cast<unsigned int>(sf::Mouse::getPosition(wnd).x - 50),
            static_cast<unsigned int>(sf::Mouse::getPosition(wnd).y - 50)
        };
    }
    // draw
    wnd.clear();
    wnd.draw(sprite[0]); // board

    for(const auto & elem : pieces)
    {
        sprite[1].setTextureRect(
            piece_im_cords[static_cast<int>(elem.figure) + 
                                         (elem.team == Team::W ? 6 : 0)]);

        sprite[1].setPosition(
            elem.piece_position.x,
            elem.piece_position.y
        );
        wnd.draw(sprite[1]);
    }

    // end the current frame
    wnd.display();
}

// Sprite can't be moved outside of the board 
void Game::sprite_in_prison() const
{
    if (sf::Mouse::getPosition(wnd).x < 0)
            sf::Mouse::setPosition(
                {0, sf::Mouse::getPosition(wnd).y}, wnd);

    if (sf::Mouse::getPosition(wnd).x > 800)
            sf::Mouse::setPosition(
                {800, sf::Mouse::getPosition(wnd).y}, wnd);

    if (sf::Mouse::getPosition(wnd).y < 0)
            sf::Mouse::setPosition(
                {sf::Mouse::getPosition(wnd).x, 0}, wnd);

    if (sf::Mouse::getPosition(wnd).y > 800)
            sf::Mouse::setPosition(
                {sf::Mouse::getPosition(wnd).x, 800}, wnd);
}

void Game::capture(std::vector<Piece> pieces)
{
    // erase elem
}

bool check_bounds(const sf::Vector2i sel_pos)
{
    // if out of bonds return true
    return (sel_pos.y > 800 || 
            sel_pos.y < 0   || 
            sel_pos.x > 800 || 
            sel_pos.x < 0);
}

bool is_check(
    const Piece * sel_piece, 
    const sf::Vector2i sel_pos)
{
    return false;
}

bool collision(
    const Piece * sel_piece, 
    const sf::Vector2i sel_pos)
{
    // raycast
    return false;
}

bool pope_move_logic(
    const sf::Vector2i sel_pos, 
    const sf::Vector2u start_pos)
{
    return 
        (abs(start_pos.x/100 - sel_pos.x/100) == abs(start_pos.y/100 - sel_pos.y/100));
}

bool rook_move_logic(
    Piece * sel_piece,
    const sf::Vector2i sel_pos, 
    const sf::Vector2u start_pos)
{    
    bool move = false;
    
    if((start_pos.y/100 != sel_pos.y/100) && (start_pos.x/100 == sel_pos.x/100) ||
       (start_pos.x/100 != sel_pos.x/100) && (start_pos.y/100 == sel_pos.y/100))
    {    
        sel_piece->is_moved = true;
        move = true;
    }
       
    return move;
}

bool queen_move_logic(
    Piece * sel_piece,
    const sf::Vector2i sel_pos, 
    const sf::Vector2u start_pos)
{
    return
        rook_move_logic(sel_piece, sel_pos, start_pos) ||
        pope_move_logic(sel_pos, start_pos);
}

bool king_move_logic(
    Piece * sel_piece, 
    const sf::Vector2i sel_pos, 
    const sf::Vector2u start_pos)
{
    bool move = false;
    
    if( ((start_pos.x/100 + 1 == sel_pos.x/100)  || 
         (start_pos.x/100 - 1 == sel_pos.x/100)  || 
         (start_pos.x/100     == sel_pos.x/100)) 
        &&
        ((start_pos.y/100 + 1 == sel_pos.y/100)  || 
         (start_pos.y/100 - 1 == sel_pos.y/100)  || 
         (start_pos.y/100     == sel_pos.y/100)))
    {    
        sel_piece->is_moved = true;
        move = true;
    }
       
    return move;
}

bool pawn_move_logic(
    Piece * sel_piece, 
    const sf::Vector2i sel_pos, 
    const sf::Vector2u start_pos)
{
    bool double_move = false;
    bool single_move = false;
    bool moved = false;


    if(sel_piece->is_moved == false) // case double move   
    {

        double_move = 
            (abs(start_pos.y/100 - sel_pos.y/100) <= 2) && 
            (sel_pos.x/100 == start_pos.x/100);
        
        // case if we click and dont care
        if(double_move && (sel_pos.y/100 != start_pos.y/100)) moved = true;

    }else{

        if(sel_piece->team == Team::B)
        {
            single_move = 
                (start_pos.y/100 - sel_pos.y/100 == 1) && 
                (sel_pos.x/100 == start_pos.x/100);
        }
        if(sel_piece->team == Team::W)
        {
            single_move = 
                (start_pos.y/100 - sel_pos.y/100 == -1) && 
                (sel_pos.x/100 == start_pos.x/100);
        }
        
        if(single_move) moved = true;
    }

    if(moved) sel_piece->is_moved = true; // disable it
    
    return single_move || double_move;
}

bool knight_move_logic(    
    const sf::Vector2i sel_pos, 
    const sf::Vector2u start_pos)
{
    return 
    (
        ((abs(start_pos.x/100 - sel_pos.x/100) == 2) && (abs(start_pos.y/100 - sel_pos.y/100) == 1)) || 
        ((abs(start_pos.x/100 - sel_pos.x/100) == 1) && (abs(start_pos.y/100 - sel_pos.y/100) == 2))
    );
}

void Game::promote_pawn(std::vector<Piece> pieces)
{

}

bool figure_logic(
    Piece * sel_piece, 
    const sf::Vector2i sel_pos, 
    const sf::Vector2u start_pos)
{   
    // divide it here to not perform it in every logic func
    std::cout << " start x : " << start_pos.x/100 << " start y : " << start_pos.y/100 << std::endl;
    switch (sel_piece->figure)
    {
        case Pcs::K: { return king_move_logic(sel_piece, sel_pos, start_pos); }
        case Pcs::Q: { return queen_move_logic(sel_piece, sel_pos, start_pos); }
        case Pcs::P: { return pawn_move_logic(sel_piece, sel_pos, start_pos); }
        case Pcs::R: { return rook_move_logic(sel_piece, sel_pos, start_pos); }
        case Pcs::N: { return knight_move_logic(sel_pos, start_pos); }
        case Pcs::B: { return pope_move_logic(sel_pos, start_pos); }

        default:
            break;
    }

    return false;
}

bool Game::move_validation(
    Piece * sel_piece,
    const sf::Vector2i sel_pos, 
    const sf::Vector2u start_pos) 
{
    // what to add
    /*
        promote pawn
        capture
        mate
        maybe move all of it to figure logic
    */ 
    return 
        figure_logic(sel_piece, sel_pos, start_pos) && 
        !collision(sel_piece, sel_pos) && 
        !is_check(sel_piece, sel_pos);
}

sf::Vector2u Game::drag(sf::Event event)
{   
    if (event.type == sf::Event::MouseButtonPressed && 
        event.mouseButton.button == sf::Mouse::Left)
    {
        sel_pos = sf::Mouse::getPosition(wnd);

        const auto predicate = [this](const Piece & piece) {
            return
                std::abs(static_cast<int>(
                    this->sel_pos.x - piece.piece_position.x - 50)) < 50 &&
                std::abs(static_cast<int>(
                    this->sel_pos.y - piece.piece_position.y - 50)) < 50;
        };

        auto it = std::find_if(pieces.begin(), pieces.end(), predicate);
        sel_piece = it != pieces.end() ? &(*it) : nullptr;
    }

    return start_pos = sf::Vector2u {
        // divide to get rid of xyz, yz values and place x in center
        static_cast<unsigned int>(sel_pos.x / 100) * 100, 
        static_cast<unsigned int>(sel_pos.y / 100) * 100
    };
}

void Game::drop(const sf::Event event)
{
    if (event.type == sf::Event::MouseButtonReleased && 
        event.mouseButton.button == sf::Mouse::Left  && 
        sel_piece != nullptr)
    {
        sel_pos = sf::Mouse::getPosition(wnd);

        if (check_bounds(sel_pos)){}
        else if (move_validation(sel_piece, sel_pos, start_pos))
        {
            //sel_pos /= 100;
            sel_piece->piece_position = sf::Vector2u {
                static_cast<unsigned int>(sel_pos.x / 100) * 100,
                static_cast<unsigned int>(sel_pos.y / 100) * 100
            };
            sel_piece = nullptr;
        }
        else
        {
            sel_piece->piece_position = sf::Vector2u {
                start_pos.x,
                start_pos.y
            };
            sel_piece = nullptr;
        }
    }
    
}




int main()
{
    Game Obj;
    Obj.run();

    return 0;
}



// TODO:
/*
    priority
        dragging piece and sliding into left || up 
        logic
        when to check tour
        selpos is vice versa 7 is 1 and should be 7 for whites
        move validation add capture in return
        in chess notation change y 6 -> 3, 1 -> 8
    further
        Game vs computer
        analyse
        Game vs human
*/
