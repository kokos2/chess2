# Chess written in C++ and SFML library
Simple simulation of classic game

## Building
``` sh
$ git clone --recursive https://gitlab.com/kokos2/chess2.git
$ mkdir build
$ cd build
$ cmake ..
$ cmake --build .
```

## Project status
* Unfinished, so far pieces movement done, need to finish up collision and game logic
* planning to manage moves by command pattern 

## Future ideas
* Add stockfish, to be able to play vs computer at different difficulties
* Analyze own chess games, even by inputing whole game by chess notation
* Player vs player

# Starting screen
![Preview](preview.png)
